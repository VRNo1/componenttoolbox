#if UNITY_EDITOR
using System.IO;
using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public class ComponentHolder
{
	public class MemberHolder
	{
		public string name;
		public System.Object val;
	}
	
	public System.Type type;
	public MemberHolder[] fieldsHolder;
	public MemberHolder[] propertiesHolder;
}

public class ComponentToolbox : EditorWindow
{
	[MenuItem("Window/ComponentToolbox")]
	static void ShowWindow()
	{
		EditorWindow.GetWindow(typeof(ComponentToolbox));
	}

	static bool _selectionIsValid = false;

	System.Type[] _unityTypes = null;

	ComponentHolder[] _components = null;

	static bool _inReorderState = false;

	int _componentIdxToReorder = -1;

	void InitUnityTypes()
	{
		if (_unityTypes != null)
		{
			return;
		}

		List<System.Type> unityTypesList = new List<System.Type>();
		System.Type[] atypes = Assembly.GetAssembly(typeof(GameObject)).GetTypes();
		foreach (System.Type t in atypes)
		{
			if (t.IsSubclassOf(typeof(Component)))
			{
				//Debug.Log (t.ToString());
				unityTypesList.Add(t);
			}
		}
		_unityTypes = unityTypesList.ToArray();
	}

	System.Type[] GetAllComponentTypes()
	{
		InitUnityTypes();
		List<System.Type> retVal = new List<System.Type>();

		retVal.AddRange(_unityTypes);

		System.Type[] types = GetType().Assembly.GetTypes();
		foreach (System.Type t in types)
		{
			if (t.IsSubclassOf(typeof(Component)))
			{
				retVal.Add(t);
			}
		}
		return retVal.ToArray();
	}

	const BindingFlags FLAGS = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;

	string ToReadableString(System.Object o)
	{
		string val = o != null ? o.ToString() : "null";
		val = val == "" ? "(empty string)" : val;

		return val;
	}

	void OnSelectionChange()
	{
		GameObject selectedGameObject = Selection.activeGameObject;
		if (selectedGameObject == null)
		{
			return;
		}

		Debug.Log ("Selected: " + selectedGameObject.name);

		PrefabType selectedPrefabType = PrefabUtility.GetPrefabType(selectedGameObject);

		//Debug.Log (selectedPrefabType.ToString());
		if (selectedPrefabType == PrefabType.None)
		{
			_selectionIsValid = true;
			_inReorderState = false;
			
//#if UNDEF
			RepopulateComponentHolders(selectedGameObject);
			Repaint();
//#endif
			
#if UNDEF
			System.Type[] allComponentTypes = GetAllComponentTypes();
			foreach (System.Type t in allComponentTypes)
			{
				//Debug.Log(t.ToString());
				Component c = selectedGameObject.GetComponent(t);
				if (c != null)
				{
					Debug.Log("   " + c.GetType().Name);
				}
			}
#endif
		}
		else
		{
			_selectionIsValid = false;
		}
	}

	void OnInspectorUpdate()
	{
		RepopulateComponentHolders(Selection.activeGameObject);
	}

	void RepopulateComponentHolders(GameObject g)
	{
		Dictionary<string,bool> SKIPPED_PROPERTIES = new Dictionary<string, bool>();
		SKIPPED_PROPERTIES.Add("active", false);
		SKIPPED_PROPERTIES.Add("tag", false);
		SKIPPED_PROPERTIES.Add("name", false);
		SKIPPED_PROPERTIES.Add("hideFlags", false);

		List<ComponentHolder> chs = new List<ComponentHolder>();
		Component[] cs = g.GetComponents<Component>();
		foreach (Component c in cs)
		{
			if (c == null || c.GetType() == typeof(Transform))
				continue;
	
			//Debug.Log("             " + c.GetType().ToString());
			ComponentHolder ch = new ComponentHolder();
			ch.type = c.GetType();
			
			List<ComponentHolder.MemberHolder> m = new List<ComponentHolder.MemberHolder>();
			FieldInfo[] fields = c.GetType().GetFields(FLAGS);
			foreach (FieldInfo f in fields)
			{
				if (f.IsPublic || f.IsDefined(typeof(SerializeField), true))
				{
					ComponentHolder.MemberHolder newm = new ComponentHolder.MemberHolder();
					newm.name = f.Name;
					newm.val = f.GetValue(c);
					m.Add(newm);
					//string val = ToReadableString(f.GetValue(c));
					//Debug.Log ("             " + "             " + f.Name + ": " + val);
				}
			}
			ch.fieldsHolder = m.ToArray();
			
			m.Clear();
			PropertyInfo[] properties = c.GetType().GetProperties(FLAGS);
			foreach (PropertyInfo p in properties)
			{
				//p.Name
				if (p.CanWrite && p.CanRead)
				{
					if (SKIPPED_PROPERTIES.ContainsKey(p.Name))
						continue;
					
					ComponentHolder.MemberHolder newm = new ComponentHolder.MemberHolder();
					newm.name = p.Name;
					newm.val = p.GetValue(c, null);
					m.Add(newm);
					//string val = ToReadableString(p.GetValue(c, null));
					//Debug.Log ("             " + "             " + p.Name + ": " + val);
				}
			}
			ch.propertiesHolder = m.ToArray();
	
			chs.Add(ch);
		}
		_components = chs.ToArray();
	}

	void SwapComponents(int src, int dest)
	{
		if (src == -1 || dest == -1)
		{
			return;
		}

		ComponentHolder temp = _components[dest];
		_components[dest] = _components[src];
		_components[src] = temp;
	}

	void Apply()
	{
		GameObject selectedGameObject = Selection.activeGameObject;
		for (int n = 0; n < _components.Length; ++n)
		{
			if (selectedGameObject.GetComponent(_components[n].type) == null)
			{
				// selected game object does not have this component?
				// we're out of sync somehow. abort.
				return;
			}
		}
		
		// remove components
		for (int n = 0; n < _components.Length; ++n)
		{
			Component c = selectedGameObject.GetComponent(_components[n].type);
			DestroyImmediate(c);
		}
		
		// reinsert new duplicates of removed components
		// reset with values the same as the old, removed ones
		for (int n = 0; n < _components.Length; ++n)
		{
			Component c = selectedGameObject.AddComponent(_components[n].type);
			foreach (var f in _components[n].fieldsHolder)
			{
				c.GetType().GetField(f.name, FLAGS).SetValue(c, f.val);
			}
			foreach (var p in _components[n].propertiesHolder)
			{
				c.GetType().GetProperty(p.name, FLAGS).SetValue(c, p.val, null);
			}
		}
	}

	void OnGUI()
	{
		if (!_selectionIsValid)
		{
			return;
		}
		

		if (_components != null && _components.Length > 0)
		{
			// really quick and dirty
			GUIStyle s = GUI.skin.GetStyle("Label");
			s.wordWrap = true;

			GUILayout.Label("Click on the component you want moved, then click on the other component you want it to switch places with.");
			for (int n = 0; n < _components.Length; ++n)
			{
				if (GUILayout.Button(_components[n].type.Name))
				{
					if (!_inReorderState)
					{
						_inReorderState = true;
						_componentIdxToReorder = n;
					}
					else
					{
						_inReorderState = false;
						if (_componentIdxToReorder != n)
						{
							SwapComponents(_componentIdxToReorder, n);
							Apply();
						}
					}
				}
			}
		}
	}
}
#endif
